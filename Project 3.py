import csv
import itertools
import math
import numpy as np
import matplotlib.pyplot as plt
import plotly.plotly as py
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
from plotly.tools import FigureFactory as FF
from rope.base.pyobjectsdef import _AssignVisitor
from scipy import stats
from sklearn import linear_model
from snowballstemmer.danish_stemmer import lab0
import sklearn.preprocessing as prepoc
import pandas as pd


np.set_printoptions(threshold=np.nan)

data_ = []  # Array which we load the data file into
feature_names = np.asarray(['age', 'sex', 'cp', 'trestbps', 'chol', 'fbs', 'restecg', 'thalach', 'exang', 'oldpeak', 'slope', 'ca', 'thal', 'num (predicted)'])

with open('processed.cleveland.data.txt', 'rb') as csv_file:
    spam_reader = csv.reader(csv_file, delimiter=' ')
    for row in spam_reader:
        data_.append(str(row[0]).split(','))
with open('processed.hungarian.data.txt', 'rb') as csv_file:
    spam_reader = csv.reader(csv_file, delimiter=' ')
    for row in spam_reader:
        data_.append(str(row[0]).split(','))
with open('processed.switzerland.data.txt', 'rb') as csv_file:
    spam_reader = csv.reader(csv_file, delimiter=' ')
    for row in spam_reader:
        data_.append(str(row[0]).split(','))
with open('processed.va.data.txt', 'rb') as csv_file:
    spam_reader = csv.reader(csv_file, delimiter=' ')
    for row in spam_reader:
        data_.append(str(row[0]).split(','))

# Pre-process Step 1: Data Integration data from the different countries are integrated to mak a whole.

# data_ = [[float(string) for string in inner] for inner in data_]  # Convert the strings to int's for easy usage.
data = np.array(data_)  # convert the data array to a numpy array to be able to use it's features.

data[data == '?'] = np.nan  # converts the ?'s to Numpy nan's for easy usage.

# Pre-process Step 2: Cleaning the data Imputing the missing data

imp = prepoc.Imputer(missing_values='NaN', copy=False, strategy='most_frequent')  # impute the missing data with the mean of the other features.
imp.fit(data)
data = imp.transform(data)
# print data

feature = []  # The i'th feature is feature[i]
for i in range(0, data.shape[1]):
    feature.append(data[:, i])

# --- Categorize the Age Feature ---

# print np.var(data[:, 0])  # 88.72
# print np.mean(data[:, 0])  # 53.51
#
# print np.min(data[:, 0])  # 28
# print np.max(data[:, 0])  # 77

data[:, 0] = pd.cut(data[:, 0], [20, 40, 50, 60, 70, 80], labels=False)  # based on intuition.

# --- Categorize the blood pressure feature ---

# print np.var(data[:, 3])  # 339.80
# print np.mean(data[:, 3])  # 132.13
# print np.min(data[:, 3][data[:, 3] != 0])  # 80
# print np.max(data[:, 3])  # 200

data[:, 3] = pd.cut(data[:, 3], [70, 90, 120, 140, 200], labels=False)  # based on medical charts.

# --- Categorize the cholesterol feature ---

# print np.var(data[:, 4][data[:, 4] != 0])  # 3371.058
# print np.mean(data[:, 4][data[:, 4] != 0])  # 244.91
# print np.min(data[:, 4][data[:, 4] != 0])  # 85
# print np.max(data[:, 4])  # 603
print data[:, 4]
data[:, 4] = pd.cut(data[:, 4], [-1, 140, 200, 240, 300, 600], labels=False)  # based on medical charts. # too many 0's not good.

# --- Categorize the maximum heart rate feature ---

# print np.var(data[:, 7])  # 631.25
# print np.mean(data[:, 7])  # 137.54
# print np.min(data[:, 7])  # 60
# print np.max(data[:, 7])  # 202
#
data[:, 7] = pd.cut(data[:, 7], 6, labels=False)   # based on intuition.
# print data[:, 7]

# --- Categorize the old peak rate feature ---

# print np.var(data[:, 9])  # 1.10
# print np.mean(data[:, 9])  # 0.87
# print np.min(data[:, 9])  # -2.6
# print np.max(data[:, 9])  # 6.2
#
data[:, 9] = pd.cut(data[:, 9], 16, labels=False)  # based on intuition.
# print data[:, 9]

# --- Change categorization of thal

# print np.var(data[:, 12])  # 1.73
# print np.mean(data[:, 12])  # 5.08
# print np.min(data[:, 12])  # 3.0
# print np.max(data[:, 12])  # 7.0

data[:, 12] = pd.cut(data[:, 12], [2, 5, 6, 7], labels=False)  # 3->0, 6->1, 7->2
# print data[:, 12]

imp = prepoc.Imputer(missing_values='NaN', copy=False, strategy='most_frequent')  # impute the missing data again because of bad values caused by discretization
imp.fit(data)
data = imp.transform(data)

print data

for i in range(0, data.shape[1]):  # Scatter plot start.
    plt.subplot(2, 7, i + 1)
    plt.title('Variable ' + str(i+1))
    plt.xlabel('Variable ' + str(i+1))
    plt.ylabel('Target Variable')
    plt.hist2d(data[:, i], data[:, 13], bins=np.max(data[:, i])+1)

plt.show()







